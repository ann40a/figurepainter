import lombok.val;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FigurePainterApp extends JFrame {
    private JPanel jPanelMain;
    private FigureManager manager;

    private FigurePainterApp() throws Exception {
        super("Figure application");
        manager = FigureManager.getInstance();
        init();
    }

    public static void main(String[] args) throws Exception {
        val app = new FigurePainterApp();
        app.setLocation(app.getCenterPoint());
        app.setVisible(true);
    }

    private void init() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jPanelMain = new JPanel() {
            protected void paintComponent(Graphics graphics) {
                manager.drawFigures((Graphics2D) graphics);
            }
        };
        jPanelMain.setFocusable(true);
        jPanelMain.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (manager.getCurrentType() != null) {
                    manager.addPoint(e.getPoint());
                    if (testNumOfPoints() == 1) {
                        addFigures();
                    }
                }
                repaint();
            }
        });
        jPanelMain.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (manager.getCurrentType() != null) {
                    addFigures();
                }
            }
        });
        Container container = getContentPane();
        container.setLayout(new BorderLayout());
        container.add(jPanelMain, BorderLayout.CENTER);
        setJMenuBar(createMenu());
        setSize(new Dimension(800, 600));
    }

    private void addFigures() {
        if (manager.getCurrentType() != null) {
            if (testNumOfPoints() == 1 || testNumOfPoints() == -1) {
                val figure = manager.createFigure();
                if (figure != null) {
                    manager.add(figure);
                }
                else {
                    showError("Unexpected error during figure creation");
                }
                manager.setCurrentType(null);
                jPanelMain.repaint();
            } else {
                showError("Wrong number of points!");
            }
        }
    }

    private int testNumOfPoints() {
        val current = manager.getCurrentNumOfPoints();
        val expected = manager.getExpectedNumberOfPoints();
        if (expected == 0) {
            return -1;
        }

        if (current == expected) {
            return 1;
        }

        return 0;
    }

    private JMenuBar createMenu() {
        JMenu figuresMenu = new JMenu("Figures");
        manager.getFigureTypes().forEach(type -> {
            val menuItem = new JMenuItem(type);
            menuItem.addActionListener(e -> {
                addFigures();
                manager.setCurrentType(type);
            });
            figuresMenu.add(menuItem);
        });

        JMenuBar menu = new JMenuBar();
        menu.add(figuresMenu);
        return menu;
    }

    private Point getCenterPoint() {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int width = getSize().width;
        int height = getSize().height;
        int x = (dimension.width - width) / 2;
        int y = (dimension.height - height) / 2;
        return new Point(x, y);
    }

    private void showError(String message) {
        JOptionPane.showMessageDialog(
                null,
                message,
                "Error",
                JOptionPane.ERROR_MESSAGE
        );
    }
}
