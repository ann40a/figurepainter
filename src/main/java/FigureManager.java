import drawing.FigureDrawer;
import figures.Figure;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import org.reflections.Reflections;

import java.awt.*;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FigureManager {
    private static final int POINT_RADIUS = 2;
    private static final String FIGURES_BASE_PACKAGE = "figures.impl";
    private static FigureManager manager;

    @Getter
    private List<String> figureTypes = new ArrayList<>();
    @Getter @Setter
    private String currentType;

    private FigureDrawer drawer;
    private Map<String, Function<Point[], Figure>> figureCreators = new HashMap<>();
    private Map<String, Integer> figureNumberOfPoints = new HashMap<>();
    private ArrayList<Figure> figures = new ArrayList<>();
    private ArrayList<Point> points = new ArrayList<>();

    public static FigureManager getInstance() throws Exception {
        if (manager == null) {
            manager = new FigureManager();
        }
        return manager;
    }

    private FigureManager() throws Exception {
        drawer = new FigureDrawer();

        val reflections = new Reflections(FIGURES_BASE_PACKAGE);
        val figuresClasses = reflections.getSubTypesOf(Figure.class).stream()
                .filter(cl -> !Modifier.isAbstract(cl.getModifiers()))
                .collect(Collectors.toList());
        for (val figureClass : figuresClasses) {
            val figureType = (String) figureClass.getField("name").get(null);
            figureTypes.add(figureType);

            val numberOfPoints = figureClass.getField("numberOfPoints").getInt(null);
            figureNumberOfPoints.put(figureType, numberOfPoints);

            val createMethod = figureClass.getMethod("create", Point[].class);
            figureCreators.put(figureType, (points) -> {
                try {
                    return (Figure) createMethod.invoke(null, new Object[] { points });
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            });
        }
        figureTypes.sort(String::compareTo);
    }

    public void addPoint(Point point) {
        points.add(point);
    }

    public int getCurrentNumOfPoints() {
        return points.size();
    }

    public int getExpectedNumberOfPoints() {
        return figureNumberOfPoints.get(currentType);
    }

    public Figure createFigure() {
        Point[] p = points.toArray(new Point[0]);
        points.clear();
        return figureCreators.get(currentType).apply(p);
    }

    public void add(Figure figure) {
        figures.add(figure);
    }

    public void drawFigures(Graphics2D graphics) {
        graphics.clearRect(0, 0, 2000, 1000);
        for (Figure figure : figures) {
            drawer.draw(figure, graphics);
        }
        graphics.setColor(Color.BLACK);
        for (Point p : points) {
            graphics.fillOval(p.x - POINT_RADIUS, p.y - POINT_RADIUS, 2 * POINT_RADIUS, 2 * POINT_RADIUS);
        }
    }
}
