package drawing;

import figures.Figure;
import lombok.val;
import org.reflections.Reflections;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class FigureDrawer {
    private static final int POINT_RADIUS = 2;
    private static final String DRAWING_STRATEGIES_BASE_PACKAGE = "drawing.impl";

    private final List<FigureDrawerStrategy> strategies;

    public FigureDrawer() throws Exception {
        val reflections = new Reflections(DRAWING_STRATEGIES_BASE_PACKAGE);
        val strategiesClasses = reflections.getSubTypesOf(FigureDrawerStrategy.class);
        strategies = new ArrayList<>();
        for (val strategyClass : strategiesClasses) {
            strategies.add(strategyClass.newInstance());
        }
    }

    public <T extends Figure> void draw(T figure, Graphics2D graphics) {
        val strategy = strategies.stream()
                .filter(st -> st.getFigureClass().isInstance(figure))
                .findAny()
                .orElseThrow(NullPointerException::new);
        this.draw(strategy, figure, graphics);
    }

    private <T extends Figure> void draw(
            FigureDrawerStrategy<T> strategy,
            T figure,
            Graphics2D graphics
    ) {
        strategy.draw(figure, graphics);
        for(Point p : strategy.getPrimaryPoints(figure)) {
            graphics.fillOval(
                    p.x - POINT_RADIUS,
                    p.y - POINT_RADIUS,
                    2 * POINT_RADIUS,
                    2 * POINT_RADIUS
            );
        }
    }
}
