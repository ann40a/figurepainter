package drawing.impl;

import drawing.FigureDrawerStrategy;
import figures.impl.ClosedPolygon;
import figures.Figure;

import java.awt.*;

public class ClosedPolygonDrawerStrategy implements FigureDrawerStrategy<ClosedPolygon> {
    @Override
    public Class<? extends Figure> getFigureClass() {
        return ClosedPolygon.class;
    }

    @Override
    public void draw(ClosedPolygon figure, Graphics2D graphics) {
        Point points[] = figure.getPoints();
        int xPoints[] = new int[points.length];
        int yPoints[] = new int[points.length];
        for (int i = 0; i < points.length; ++i) {
            xPoints[i] = points[i].x;
            yPoints[i] = points[i].y;
        }
        graphics.drawPolygon(xPoints, yPoints, points.length);
    }

    @Override
    public Point[] getPrimaryPoints(ClosedPolygon figure) {
        return figure.getPoints();
    }
}
