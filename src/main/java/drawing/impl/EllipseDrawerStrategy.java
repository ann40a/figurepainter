package drawing.impl;

import drawing.FigureDrawerStrategy;
import figures.impl.Ellipse;
import figures.Figure;

import java.awt.*;

public class EllipseDrawerStrategy implements FigureDrawerStrategy<Ellipse> {
    @Override
    public Class<? extends Figure> getFigureClass() {
        return Ellipse.class;
    }

    @Override
    public void draw(Ellipse figure, Graphics2D graphics) {
        Point center = figure.getCenter();
        Point radius = figure.getRadius();
        graphics.drawOval(
                center.x - radius.x,
                center.y - radius.y,
                2 * radius.x,
                2 * radius.y
        );
    }

    @Override
    public Point[] getPrimaryPoints(Ellipse figure) {
        return new Point[] { figure.getCenter() };
    }
}
