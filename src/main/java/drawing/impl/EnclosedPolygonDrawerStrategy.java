package drawing.impl;

import drawing.FigureDrawerStrategy;
import figures.impl.EnclosedPolygon;
import figures.Figure;

import java.awt.*;

public class EnclosedPolygonDrawerStrategy implements FigureDrawerStrategy<EnclosedPolygon> {
    @Override
    public Class<? extends Figure> getFigureClass() {
        return EnclosedPolygon.class;
    }

    @Override
    public void draw(EnclosedPolygon figure, Graphics2D graphics) {
        Point points[] = figure.getPoints();
        for(int i = 0; i < points.length - 1; ++i) {
            graphics.drawLine(points[i].x, points[i].y, points[i+1].x, points[i+1].y);
        }
    }

    @Override
    public Point[] getPrimaryPoints(EnclosedPolygon figure) {
        return figure.getPoints();
    }
}
