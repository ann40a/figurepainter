package drawing;

import figures.Figure;

import java.awt.*;

public interface FigureDrawerStrategy<T extends Figure> {
    Class<? extends Figure> getFigureClass();

    void draw(T figure, Graphics2D graphics);

    Point[] getPrimaryPoints(T figure);
}
