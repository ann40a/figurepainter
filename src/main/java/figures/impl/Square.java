package figures.impl;

import figures.Figure;

import java.awt.*;

public class Square extends Rectangle {
    public static final String name = "Square";
    public static final int numberOfPoints = 2;

    public static Figure create(Point... points) {
        return new Square(points[0], points[1]);
    }

    public Square(Point p1, Point p2) {
        super(
                p1,
                p2,
                new Point(
                        p2.x - p1.y + p2.y,
                        p2.y + p1.x - p2.x
                )
        );
    }
}