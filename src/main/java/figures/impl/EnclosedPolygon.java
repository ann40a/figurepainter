package figures.impl;

import figures.Figure;
import figures.Polygon;

import java.awt.*;

public class EnclosedPolygon extends Polygon {
    public static final String name = "Enclosed Polygon";
    public static final int numberOfPoints = 0;

    public static Figure create(Point... points) {
        return new EnclosedPolygon(points);
    }

    public EnclosedPolygon(Point... points) {
        super(points);
    }
}
