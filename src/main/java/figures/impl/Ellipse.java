package figures.impl;

import figures.Figure;
import lombok.Data;

import java.awt.*;

@Data
public class Ellipse extends Figure {
    public static final String name = "Ellipse";
    public static final int numberOfPoints = 2;

    public static Figure create(Point... points) {
        return new Ellipse(points[0], points[1]);
    }

    private Point center;
    private Point radius;

    public Ellipse(Point p0, Point p1) {
        this.setCenter(new Point(
                Math.abs(p1.x + p0.x) / 2,
                Math.abs(p1.y + p0.y) / 2
        ));
        this.setRadius(new Point(
                Math.abs(p1.x - p0.x) / 2,
                Math.abs(p1.y - p0.y) / 2
        ));
    }
}