package figures.impl;

import figures.Figure;

import java.awt.*;

public class RectangularTriangle extends ClosedPolygon {
    public static final String name = "Rectangular Triangle";
    public static final int numberOfPoints = 2;

    public static Figure create(Point... points) {
        return new RectangularTriangle(points[0], points[1]);
    }

    public RectangularTriangle(Point p0, Point p1) {
        super(
                p0,
                new Point(p1.x, p0.y),
                p1
        );
    }
}