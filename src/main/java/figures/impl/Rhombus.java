package figures.impl;

import figures.Figure;

import java.awt.*;

public class Rhombus extends Parallelogram {
    public static final String name = "Rhombus";
    public static final int numberOfPoints = 3;

    public static Figure create(Point... points) {
        return new Rhombus(points[0], points[1], points[2]);
    }

    public Rhombus(Point p1, Point p2, Point p3) {
        super(
                p1,
                p2,
                new Point(
                        p2.x + (int)(p1.distance(p2) * (p3.x - p2.x) / p2.distance(p3)),
                        p2.y + (int)(p1.distance(p2) * (p3.y - p2.y) / p2.distance(p3))
                )
        );
    }
}