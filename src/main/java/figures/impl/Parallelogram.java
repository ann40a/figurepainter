package figures.impl;

import figures.Figure;

import java.awt.*;

public class Parallelogram extends ClosedPolygon {
    public static final String name = "Parallelogram";
    public static final int numberOfPoints = 3;

    public static Figure create(Point... points) {
        return new Parallelogram(points[0], points[1], points[2]);
    }

    public Parallelogram(Point p0, Point p1, Point p2) {
        super(
                p0,
                p1,
                p2,
                new Point(
                        p2.x + p0.x - p1.x,
                        p2.y + p0.y - p1.y
                )
        );
    }
}