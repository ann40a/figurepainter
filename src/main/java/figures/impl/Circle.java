package figures.impl;

import figures.Figure;

import java.awt.*;

public class Circle extends Ellipse {
    public static final String name = "Circle";
    public static final int numberOfPoints = 2;

    public static Figure create(Point... points) {
        return new Circle(points[0], points[1]);
    }

    public Circle(Point center, Point p) {
        super(
                new Point(
                        center.x - (int)center.distance(p),
                        center.y - (int)center.distance(p)
                ),
                new Point(
                        center.x + (int)center.distance(p),
                        center.y + (int)center.distance(p)
                )
        );
    }
}