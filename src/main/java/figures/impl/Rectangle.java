package figures.impl;

import figures.Figure;

import java.awt.*;

public class Rectangle extends Parallelogram {
    public static final String name = "Rectangle";
    public static final int numberOfPoints = 3;

    public static Figure create(Point... points) {
        return new Rectangle(points[0], points[1], points[2]);
    }

    public Rectangle(Point p0, Point p1, Point p2) {
        super(
                p0,
                p1,
                getThirdPoint(p0, p1, p2)
        );
    }

    /*
    * x + by + c1 = 0
    * bx - y + c2 = 0
    * x + by + c3 = 0
    * */
    private static Point getThirdPoint(Point p0, Point p1, Point p2) {
        double b = 1.0 * (p0.x - p1.x) / (p1.y - p0.y);
        double c2 = p1.y - b * p1.x;
        double c3 = - p2.x - b * p2.y;
        double x = (-b * c2 - c3) / (b * b + 1);
        double y = -(c3 + x) / b;

        return new Point((int) x, (int) y);
    }
}