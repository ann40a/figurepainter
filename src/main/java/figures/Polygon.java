package figures;

import lombok.Data;

import java.awt.*;

@Data
public abstract class Polygon extends Figure {
    private Point[] points;

    public Polygon(Point... points) {
        this.points = points;
    }
}