## Perspective technologies of object-oriented programming
### Labs 1 - 2 implementation notes
Figure Painter supports 9 different figures like `Ellipse`, `Circle`, `Closed` and `Enclosed` polygons, `Square`, etc.  
To start drawing select a figure from menu. Figures that can be drawn with a certain number of points (e.g. Circle) will be drawn automatically. For figures that can contain any number of points (like Closed and Enclosed polygons) press any key (e.g. enter) to start drawing.  
[Screenshot](http://prntscr.com/izmzcw)

Implementation classes are loaded using reflection. Every figure class should contain `name` static field and `create(Point... points)` static method. There is a way of avoiding static methods by using special plugin classes instead of them. But as I implemented this way in Labs 3 - 6, I decided not to repeat myself and implement something different in this lab.

Every figure class should either has appropriate `FigureDrawerStrategy` implementation or extends parent class (like `ClosedPolygon`) that already contains this implementation.
